# HOLALA - Live Random Premium Video Calls - Voice Calls

HOLALA is the best free live random video, voice, text chat app for your friends and strangers worldwide! Just install, open the app and start meeting people from all over the world, for free!
Live and free instant video chat with people from all over the world!

## Features 

- Free Live Video Chat 
- Free Live Voice Chat 
- Free Text Chat 
- 1-on-1 Direct Video Call 
- Select the region and gender of your choice 
- Neaby mode 
- History Searching 
- Meet people all over the world and more

## Requirements

- Minimum Android Target is V 5.0 and up.
- Compile Sdk Version 29

## Screnshot

![alt Text](screenshot/screenshot.png "screenshot")


## Support

[Rodrigo](mailto:silverio.developer@gmail.com)
